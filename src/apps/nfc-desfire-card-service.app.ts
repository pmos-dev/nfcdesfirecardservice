import * as LibFreefare from 'freefare';

import { TPropertyObject } from 'tscommons-core';
import { CommonsAsync } from 'tscommons-async';

import { CommonsOutput } from 'nodecommons-cli';
import { CommonsApp } from 'nodecommons-app';

import { INfcDesfireFile } from 'nfccommons-core';
import { NfcFreefare } from 'nfccommons-freefare';
import { NfcDevice } from 'nfccommons-freefare';
import { NfcContinuousScannerDesfireString } from 'nfccommons-freefare';
import { NfcDummy, EDummyType } from 'nfccommons-freefare';

import { IpcServer } from '../servers/ipc.server';

import { isTServiceConfig } from '../types/tservice-config';
import { isTNfcConfig } from '../types/tnfc-config';

class InternalNfcContinuousScannerDesfireString extends NfcContinuousScannerDesfireString {
	constructor(
			private ipcServer: IpcServer,
			device: NfcDevice,
			file: INfcDesfireFile,
			useCache: boolean,
			private cacheCallback?: (uid: string, data: string) => void
	) {
		super(device, file, useCache);
	}
	
	protected async handleData(uid: string, data: string): Promise<void> {
		CommonsOutput.debug(`Detected: ${uid} ${data}`);
		this.ipcServer.detected(uid, data);
		
		if (this.cacheCallback) this.cacheCallback(uid, data);
	}

	protected async start(): Promise<void> {
		// do nothing
	}

	protected async stop(): Promise<void> {
		// do nothing
	}
}

export class NfcDesfireCardServiceApp extends CommonsApp {
	private credentials: INfcDesfireFile|undefined;

	private freefare: NfcFreefare|undefined = undefined;
	private device: NfcDevice|undefined = undefined;
	private scanner: InternalNfcContinuousScannerDesfireString|undefined = undefined;

	private ipcServer: IpcServer;
	
	private dummyNfc?: NfcDummy;
	
	private masterCache: Map<string, string> = new Map<string, string>();
	
	constructor(
			private useCache: boolean,
			private startEnabled: boolean,
			private pollForDevices: boolean,
			private dummy: boolean,
			private deviceName?: string,
			private socketName?: string
	) {
		super('nfc-desfire-card-service');
		
		const serviceConfig: TPropertyObject = this.getConfigArea('service');
		if (!isTServiceConfig(serviceConfig)) throw new Error('Service config is not valid');
		
		this.useCache = useCache || serviceConfig.useCache || false;
		if (this.useCache) CommonsOutput.debug(`Using cache for tag UID scans where available`);
		
		this.socketName = this.socketName || serviceConfig.socketName || 'nfc-desfire-card-service';
		this.startEnabled = this.startEnabled || serviceConfig.startEnabled || false;
		
		const nfcConfig: TPropertyObject = this.getConfigArea('nfc');
		if (!isTNfcConfig(nfcConfig)) throw new Error('NFC config is not valid');

		if (nfcConfig.device && !this.deviceName) this.deviceName = nfcConfig.device;
		this.pollForDevices = this.pollForDevices || nfcConfig.pollForDevices || false;
		
		CommonsOutput.debug(`Creating IPC server using name: ${this.socketName}`);
		this.ipcServer = new IpcServer(this.socketName);
	}
	
	public installSystemd(): boolean {
		try {
			super.installSystemd(
					'/usr/bin/nfc-desfire-card-service --poll',
					'Node.js NFC DESFire card service',
					'nfc-desfire-card',
					true,
					'always',
					2,
					true,
					true,
					'nfc-desfire-card'
			);
			
			return true;
		} catch (e) {
			CommonsOutput.error(e.message);
			return false;
		}
	}
	
	public uninstallSystemd(): boolean {
		try {
			super.uninstallSystemd(
					'nfc-desfire-card',
					'nfc-desfire-card'
			);
			
			return true;
		} catch (e) {
			CommonsOutput.error(e.message);
			return false;
		}
	}

	public setCredentials(credentials: INfcDesfireFile): void {
		this.credentials = credentials;
	}
	
	private async detectDevice(): Promise<NfcDevice|undefined> {
		if (!this.freefare) return undefined;
		
		CommonsOutput.doing('Searching for device');
		const devices: NfcDevice[] = await this.freefare.listDevices();
		
		if (devices.length === 0) return undefined;
		
		if (this.deviceName !== undefined) {
			const device: NfcDevice|undefined = devices
					.find((d: NfcDevice): boolean => d.name === this.deviceName);
			
			if (device) {
				CommonsOutput.success();
				CommonsOutput.debug(`Selected device ${device.name}`);
			}
			
			return device;
		} else {
			CommonsOutput.result(devices.length);
			if (devices.length > 1) CommonsOutput.debug('More than one device matched. Will only use the first one.');
			
			const device: NfcDevice = devices[0];
			
			CommonsOutput.debug(`Selected device ${device.name}`);
			return device;
		}

	}
	
	protected async init(): Promise<void> {
		CommonsOutput.doing('Initialising freefare');
		try {
			if (this.dummy) {
				this.dummyNfc = new NfcDummy(EDummyType.DESFIRE);
				this.freefare = new NfcFreefare(this.dummyNfc);
			} else {
				this.freefare = new NfcFreefare(new LibFreefare());
			}
			
			if (!this.freefare) throw new Error('Unable to instantiate freefare');
		} catch (e) {
			CommonsOutput.fail(e.message);
			process.exit(3);
			throw new Error('');	// unreachable
		}
		
		CommonsOutput.success();
		
		const device: NfcDevice|undefined = await this.detectDevice();
		if (!device) {
			CommonsOutput.fail('No NFC devices available');
			
			if (!this.pollForDevices) {
				process.exit(4);
				throw new Error('');	// unreachable
			}
		}

		this.device = device;
		
		super.init();
	}
	
	protected async run(): Promise<void> {
		if (!this.freefare) throw new Error('Reached run without expected variable. This should not be possible');
		if (!this.credentials) throw new Error('No credentials have been set yet');
		
		await this.ipcServer.listen(
				(data: string, _): void => {
					if (data === 'ENABLE') {
						if (!this.scanner) return;
						
						CommonsOutput.debug('Enabling scanning');
						this.scanner.setScanningEnabled(true);
						return;
					}
					
					if (data === 'DISABLE') {
						if (!this.scanner) return;
						
						CommonsOutput.debug('Disabling scanning');
						this.scanner.setScanningEnabled(false);
						return;
					}
					
					if (data === 'ABORT') {
						if (!this.scanner) return;
						
						CommonsOutput.debug('Abort command received from client');
						this.abort();
						return;
					}
					
					CommonsOutput.error('Received unknown command from client', data);
				}
		);
		
		CommonsOutput.info(`UNIX domain socket listening on: ${this.socketName}`);
		
		while (!this.isAborted()) {
			if (!this.device) {
				if (!this.pollForDevices) return;	// this shouldn't be possible, but no harm having it in here

				CommonsOutput.debug('No device available. Polling after 5 seconds');
				
				try {
					await CommonsAsync.timeout(5000, 'nfc-desfire-card-service-poll');
				} catch (e) {
					// ignore
				}
				
				if (this.isAborted()) break;
				
				this.device = await this.detectDevice();
				
				if (!this.device) continue;
			}
			
			CommonsOutput.debug('Creating internal continuous scanner instance');
			this.scanner = new InternalNfcContinuousScannerDesfireString(
					this.ipcServer,
					this.device,
					this.credentials,
					this.useCache,
					this.useCache
							? (uid: string, data: string): void => {
								this.masterCache.set(uid, data);
							}
							: undefined
			);
			
			if (this.useCache) this.scanner.importCache(this.masterCache);
			
			CommonsOutput.info('Starting scanner service');
			
			if (this.dummy) {
				const dummyInterval: any = setInterval(
						(): void => {
							if (this.isAborted()) {
								clearInterval(dummyInterval);
								return;
							}
							
							if (!this.dummyNfc) return;
							
							this.dummyNfc.addTag({
									aid: this.credentials!.aid[0] * 65536 + this.credentials!.aid[1] * 256 + this.credentials!.aid[2],
									fileId: this.credentials!.file,
									data: '01234567890'
							});
							
							setTimeout(
									(): void => {
										if (!this.dummyNfc) return;
										
										this.dummyNfc.clearTags();
									},
									1000
							);
						},
						5000
				);
			}
			
			if (this.startEnabled) CommonsOutput.debug('Starting with scanning enabled');
			this.scanner.setScanningEnabled(this.startEnabled);
			
			await this.scanner.run();
		}
	}
	
	public abort(): void {
		super.abort();
		
		CommonsAsync.abortTimeout('nfc-desfire-card-service-poll');
		
		if (this.scanner) this.scanner.abort();
		this.scanner = undefined;
	}

	protected async shutdown(): Promise<void> {
		await this.ipcServer.shutdown();
		this.ipcServer.closeServer();

		CommonsAsync.abortTimeout('nfc-desfire-card-service-poll');
		
		if (this.scanner) this.scanner.abort();
	}
}
