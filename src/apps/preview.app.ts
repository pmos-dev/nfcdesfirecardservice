import { TPropertyObject } from 'tscommons-core';
import { CommonsAsync } from 'tscommons-async';

import { CommonsApp } from 'nodecommons-app';

import { isTServiceConfig } from '../types/tservice-config';

import { IpcClient } from '../clients/ipc.client';

export class PreviewApp extends CommonsApp {
	private ipcClient: IpcClient;

	constructor(
			socketName?: string
	) {
		super('nfc-desfire-card-service');
	
		const serviceConfig: TPropertyObject = this.getConfigArea('service');
		if (!isTServiceConfig(serviceConfig)) throw new Error('Service config is not valid');
		
		socketName = socketName || serviceConfig.socketName || 'nfc-desfire-card-service';

		this.ipcClient = new IpcClient(socketName);
	}

	protected async run(): Promise<void> {
		await super.run();

		this.ipcClient.listen(
				(data: string): void => {
					console.log(data);
				}
		);

		while (!this.isAborted()) {
			try {
				await CommonsAsync.timeout(500);
			} catch (e) {
				// ignore
			}
		}
	}
}
