import { CommonsOutput } from 'nodecommons-cli';
import { CommonsIpcUnixSocketAutoReconnectClient } from 'nodecommons-process';

export class IpcClient extends CommonsIpcUnixSocketAutoReconnectClient {
	constructor(name: string) {
		super(
				name,
				10,
				1000,
				1000,
				'_noop',
				(): void => {
					CommonsOutput.error('Failure callback fired for IPC');
				},
				(state: boolean): void => {
					if (state) CommonsOutput.completed('Connected to IPC');
					else CommonsOutput.alert('Disconnected from IPC');
				}
		);
	}
}
