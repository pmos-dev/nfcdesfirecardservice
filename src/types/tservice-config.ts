import { CommonsType } from 'tscommons-core';

export type TServiceConfig = {
		socketName: string;
		useCache?: boolean;
		startEnabled?: boolean;
};

export function isTServiceConfig(test: any): test is TServiceConfig {
	if (!CommonsType.hasPropertyStringOrUndefined(test, 'socketName')) return false;
	if (!CommonsType.hasPropertyBooleanOrUndefined(test, 'useCache')) return false;
	if (!CommonsType.hasPropertyBooleanOrUndefined(test, 'startEnabled')) return false;

	return true;
}
