import { CommonsType } from 'tscommons-core';

export type TNfcConfig = {
		device?: string;
		pollForDevices?: boolean;
};

export function isTNfcConfig(test: any): test is TNfcConfig {
	if (!CommonsType.hasPropertyStringOrUndefined(test, 'device')) return false;
	if (!CommonsType.hasPropertyBooleanOrUndefined(test, 'pollForDevices')) return false;

	return true;
}
