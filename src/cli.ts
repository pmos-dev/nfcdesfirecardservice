#!/usr/bin/env node

import { TPropertyObject } from 'tscommons-core';
import { CommonsConfig } from 'tscommons-config';

import { CommonsOutput } from 'nodecommons-cli';
import { CommonsArgs } from 'nodecommons-cli';
import { CommonsApp } from 'nodecommons-app';

import { isINfcDesfireFile } from 'nfccommons-core';
import { NfcDesfire } from 'nfccommons-freefare';

import { NfcDesfireCardServiceApp } from './apps/nfc-desfire-card-service.app';
import { PreviewApp } from './apps/preview.app';

const args: CommonsArgs = new CommonsArgs();

if (args.hasAttribute('debug')) CommonsOutput.setDebugging(true);

const systemd: boolean = args.hasProperty('systemd', 'string');

const socketName: string|undefined = args.getStringOrUndefined('socket');

const preview: boolean = args.hasAttribute('preview');

let app: CommonsApp|undefined;
let appName: string|undefined;

if (preview) {
	app = new PreviewApp(socketName);
	
	appName = 'NFC DESFire card preview';
} else {
	const useCache: boolean = args.hasAttribute('cache');
	const device: string|undefined = args.getStringOrUndefined('device');
	const startEnabled: boolean = args.hasAttribute('enabled');
	const pollForDevices: boolean = args.hasAttribute('poll');
	
	const dummy: boolean = args.hasAttribute('dummy');
	if (dummy) CommonsOutput.alert(`Using dummy freefare implementation`);
	
	app = new NfcDesfireCardServiceApp(
			useCache,
			startEnabled,
			pollForDevices,
			dummy,
			device,
			socketName
	);

	if (systemd) {
		const direction: string = args.getString('systemd');
		
		switch (direction) {
			case 'install':
				CommonsOutput.doing('Installing systemd service');
				if ((app as NfcDesfireCardServiceApp).installSystemd()) CommonsOutput.success();
				else CommonsOutput.fail();
				break;
			case 'uninstall':
				CommonsOutput.doing('Uninstalling systemd service');
				if ((app as NfcDesfireCardServiceApp).uninstallSystemd()) CommonsOutput.success();
				else CommonsOutput.fail();
				break;
			default:
				CommonsOutput.error('Unknown command for --systemd. Use either install or uninstall');
				process.exit(1);
		}
		
		process.exit(0);
	}
	
	let desfireCredentials: TPropertyObject|undefined;
	if (args.hasProperty('credentials', 'string')) {
		const credentialsFile: string = args.getString('credentials');
		desfireCredentials = NfcDesfire.readDesfireFile(credentialsFile);
	} else {
		const configDesfire: CommonsConfig = app.loadConfigFile('desfire-credentials.json');
		desfireCredentials = configDesfire.getObject('read');
	}
	
	if (!desfireCredentials || !isINfcDesfireFile(desfireCredentials)) throw new Error('NFC DESFire credentials config is not valid');
	
	(app as NfcDesfireCardServiceApp).setCredentials(desfireCredentials);
	
	appName = 'NFC DESFire card service';
}

(async (): Promise<void> => {
	CommonsOutput.debug(`Starting application: ${appName}`);
	await app.start();
	CommonsOutput.debug('Application completed');
	
	setTimeout((): void => {
		//log() // logs out active handles that are keeping node running
		process.exit(0);
	}, 100);
})();
