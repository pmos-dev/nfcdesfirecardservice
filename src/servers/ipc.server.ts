import { CommonsIpcUnixSocketAutoReconnectHost } from 'nodecommons-process';

export class IpcServer extends CommonsIpcUnixSocketAutoReconnectHost {
	constructor(name: string) {
		super(name, '775');
	}
	
	public async detected(uid: string, data: string): Promise<void> {
		await this.send(`DETECTED ${uid} ${data}`);
	}
}
