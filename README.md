# Welcome to nfc-desfire-card-service 👋
[![Version](https://img.shields.io/npm/v/nfc-desfire-card-service.svg)](https://www.npmjs.com/package/nfc-desfire-card-service)
[![License: ISC](https://img.shields.io/badge/License-ISC-yellow.svg)](#)

> NFC DESFire card scanner service

### 🏠 [Homepage](https://bitbucket.org/pm-commons-npm/nfcdesfirecardservice)

## Install

```bash
sudo npm_config_user=root npm install -g nfc-desfire-card-service
```

### Install as a systemd service:

Create the file /etc/nfc-desfire-card-service/desfire-credentials.json prior to running this.

```bash
sudo nfc-desfire-card-service --systemd=install
```

*This will also create a new nfc-desfire-card Linux group, and add the current user to it.*

### Uninstall systemd service:

```bash
sudo nfc-desfire-card-service --systemd=uninstall
```

*This will also remove the nfc-desfire-card Linux group, and remove the current user from it.*

## Usage

```bash
nfc-desfire-card-service [--cache] [--enabled] [--poll] [--device="devicename"] [--socket="socketname"] [--credentials="filename"]

```
```bash
NFC_DESFIRE_CARD_SERVICE_CONFIG="path" nfc-desfire-card-service
```

### To connect to systemd service and preview scans

```bash
nfc-desfire-card-service --preview
```

## Options (cli / nfc-desfire-card-service.json)

##### --cache / { "service": { "useCache": true } }

Enabled internal UID/data caching. This saves the tag having to be opened and read for previously seen tags, but means that changes to the card data will not be considered.

##### --enabled / { "service": { "startEnabled": true } }

Starts the service in scanning mode immediately rather than waiting for a client to instruct the service to start scanning.

##### --poll / { "nfc": { "pollForDevices": true } }

Causes the service to wait for devices to become available rather than exiting if a device is not present.

*The service also waits for further devices when the present device is lost, however in actuality the underlying LibNFC/Freefare process generally segfaults and aborts the service.* 

##### --device="devicename" / { "nfc": { "device": "devicename" } }

Specifies the name of the device to use. Other available devices are ignored.

##### --credentials="filename"

The path and filename of the credentials file in JSON format. If this is not specified, the file `desfire-credentials.json` in the config directory is used.

## Author

👤 **Pete Morris**


## Show your support

Give a ⭐️ if this project helped you!


***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_